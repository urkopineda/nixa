# Nixa - Tractiv Activity app

This project is made using **React** framework (initialized with `create-react-app`). The **serverless backend is Firebase**, and we use **`redux` and `saga`** for general state and asynchronous tasks. For **routing** between pages, we use **`react-router-dom`**.

Data is located in a **Firestore database**.

## Important notes

* When **entering** the website, the code automatically creates an **anonymous user** to link and store the data; so, all the data will be matched with you even closing the website. But, **if you clear website's cache or entering from another device will unmatch your user information**.
* This website is prepared to be shown emulating `iPhone 6/7/8` in Chrome.
* This repository uses `git-flow` for feature, release, hotfix and other GIT management solutions. Please, check branches, tags and issues ([mentioned in the next section](#progress-of-the-project)) for more information about the development of the project.

## Progress of the project

Check [issues](https://github.com/urkopineda/nixa/issues) on GitHub for more information.

* ✅ [Initial configuration of the project](https://github.com/urkopineda/nixa/issues/1).
* ✅ [US01: Home Screen - First run](https://github.com/urkopineda/nixa/issues/2).
* ✅ [US02: Home Screen ‐ Activity History](https://github.com/urkopineda/nixa/issues/3).
* ✅ [US03: Home Screen ‐ Scheduled activities](https://github.com/urkopineda/nixa/issues/4).
* ✅ [US04: Tracking activity](https://github.com/urkopineda/nixa/issues/5).
* ✅ [US04: Scheduling Activity](https://github.com/urkopineda/nixa/issues/6).
* ✅ [US04: Scheduling Activity – Finding free slots](https://github.com/urkopineda/nixa/issues/7).

## Website of the project

The project is hosted in [https://urkopineda-nixa.firebaseapp.com/](https://urkopineda-nixa.firebaseapp.com/) using **Firebase Hosting**.

## Prerequisites

You have to install `yarn`. If you don't have it already, install `node` and `npm`, then execute `npm i -g yarn`.

**Before** running the project or deploying it, **run `yarn`** on the root of the project.

To be able to **connect to Firebase**, you have to **create a `.env` file** in the root of the project. Use **`.env.sample` as template**.

## Running the project in `localhost`

Just execute `yarn start`.

## Testing

Just execute `yarn test`.

## Creating a production build

Just execute `yarn build`.

## Deploying to Firebase

First, you have to install `firebase-cli` using `npm i -g firebase-cli` and you have to run `firebase login`.

Then, just execute `yarn build` and `yarn deploy` or `firebase deploy`.
