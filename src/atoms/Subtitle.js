import React from 'react'
import PropTypes from 'prop-types'
import './Subtitle.css'

const Subtitle = ({ children }) => (
  <p className="s-subtitle">
    {children}
  </p>
)

Subtitle.propTypes = {
  children: PropTypes.node.isRequired
}

export default Subtitle
