import React from 'react'
import PropTypes from 'prop-types'
import './Card.css'

const Card = ({ image, icon, title, subtitle, onClick }) => (
  <div className="card" onClick={onClick}>
    <img className="card-image" src={image} alt={title} />
    <div className="card-content">
      <div className="card-icon-container">
        <img className="card-icon" src={icon} alt={title} />
      </div>
      <h3 className="card-title">
        {title}
      </h3>
      <p className="card-subtitle">
        {subtitle}
      </p>
    </div>
  </div>
)

Card.propTypes = {
  image: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
}

export default Card
