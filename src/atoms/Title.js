import React from 'react'
import PropTypes from 'prop-types'
import './Title.css'

const Title = ({ children, centered }) => {
  let c = 't-title'
  if (centered) {
    c += ' t-title-centered'
  }
  return (
    <h2 className={c}>
      {children}
    </h2>
  )
}

Title.propTypes = {
  children: PropTypes.node.isRequired
}

export default Title
