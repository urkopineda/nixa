import {
  types,
} from '../actions'

const initialState = {
  loading: false,
  success: false,
  failure: false,
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
  case types.INIT.REQUEST:
    return {
      ...state,
      loading: true,
      success: false,
      failure: false,
    }
  case types.INIT.SUCCESS:
    return {
      ...state,
      loading: false,
      success: true,
      failure: false,
    }
  case types.INIT.FAILURE:
    return {
      ...state,
      loading: false,
      success: false,
      failure: true,
    }
  default:
    return state
  }
}
