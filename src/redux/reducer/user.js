import {
  types,
} from '../actions'

const initialState = {
  user: undefined
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
  case types.USER.SAVE:
    return {
      ...state,
      user: action.data
    }
  default:
    return state
  }
}
