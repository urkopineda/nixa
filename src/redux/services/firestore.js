import firebase from '../firebase'

const get = ({ collection, id }) => new Promise((resolve, reject) => {
  firebase.firestore().collection(collection).doc(id).get()
    .then((doc) => {
      resolve(doc.data())
    })
    .catch((error) =>
      reject(error)
    )
})

const set = ({ collection, id, data }) =>
  firebase.firestore().collection(collection).doc(id).set(data)

const update = ({ collection, id, data }) =>
  firebase.firestore().collection(collection).doc(id).update(data)

export default {
  get,
  set,
  update
}
