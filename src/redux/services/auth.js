import firebase from '../firebase'

const signup = () => firebase.auth().signInAnonymously()

export default {
  signup
}
