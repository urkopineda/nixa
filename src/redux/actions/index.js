export const types = {
  INIT: {
    REQUEST: 'INIT.REQUEST',
    SUCCESS: 'INIT.SUCCESS',
    FAILURE: 'INIT.FAILURE',
  },
  USER: {
    SAVE: 'USER.SAVE',
  },
  ACTIVITY: {
    TRACK: {
      REQUEST: 'ACTIVITY.TRACK.REQUEST',
      SUCCESS: 'ACTIVITY.TRACK.SUCCESS',
      FAILURE: 'ACTIVITY.TRACK.FAILURE',
    },
    SCHEDULE: {
      REQUEST: 'ACTIVITY.SCHEDULE.REQUEST',
      SUCCESS: 'ACTIVITY.SCHEDULE.SUCCESS',
      FAILURE: 'ACTIVITY.SCHEDULE.FAILURE',
    }
  }
}

export const initRequest = () => ({
  type: types.INIT.REQUEST,
})

export const initSuccess = () => ({
  type: types.INIT.SUCCESS,
})


export const initFailure = (error) => ({
  type: types.INIT.FAILURE,
  error
})

export const userSave = (data) => ({
  type: types.USER.SAVE,
  data,
})

export const activityTrackRequest = (user, t, seconds) => ({
  type: types.ACTIVITY.TRACK.REQUEST,
  user,
  t,
  seconds
})

export const activityTrackSuccess = () => ({
  type: types.ACTIVITY.TRACK.SUCCESS,
})

export const activityTrackFailure = (error) => ({
  type: types.ACTIVITY.TRACK.FAILURE,
  error
})

export const activityScheduleRequest = (user, t, date, seconds) => ({
  type: types.ACTIVITY.SCHEDULE.REQUEST,
  user,
  t,
  date,
  seconds
})

export const activityScheduleSuccess = () => ({
  type: types.ACTIVITY.SCHEDULE.SUCCESS,
})

export const activityScheduleFailure = (error) => ({
  type: types.ACTIVITY.SCHEDULE.FAILURE,
  error
})
