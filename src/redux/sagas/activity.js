import firebase from 'firebase/app'

import {
  all,
  call,
  put,
  takeEvery,
} from 'redux-saga/effects'

import {
  types,
  activityTrackSuccess,
  activityTrackFailure,
  userSave
} from '../actions'

import firestore from '../services/firestore'
import utils from '../../utils'

const MINIMUM_SECONDS = 60

function* activityTrackSaga({ user, t: type, seconds }) {
  try {
    if (seconds >= MINIMUM_SECONDS) {
      const id = user.id
      if (!user.activities.length && !user.scheduled.length) {
        yield call(firestore.set, {
          collection: 'activities',
          id,
          data: {
            activities: []
          }
        })
      }
      yield call(firestore.update, {
        collection: 'activities',
        id,
        data: {
          activities: firebase.firestore.FieldValue.arrayUnion({
            type,
            timestamp: new Date(new Date().getTime() - (seconds * 1000)),
            seconds
          })
        }
      })
      const activities = yield call(firestore.get, {
        collection: 'activities',
        id,
      })
      const newUser = utils.getUser(id, activities.activities)
      yield put(userSave(newUser))
    }
    yield put(activityTrackSuccess())
  } catch (error) {
    yield put(activityTrackFailure(error))
  }
}

function* activityScheduleSaga({ user, t: type, date, seconds }) {
  try {
    const id = user.id
    if (!user.activities.length && !user.scheduled.length) {
      yield call(firestore.set, {
        collection: 'activities',
        id,
        data: {
          activities: []
        }
      })
    }
    yield call(firestore.update, {
      collection: 'activities',
      id,
      data: {
        activities: firebase.firestore.FieldValue.arrayUnion({
          type,
          timestamp: date,
          seconds
        })
      }
    })
    const activities = yield call(firestore.get, {
      collection: 'activities',
      id,
    })
    const newUser = utils.getUser(id, activities.activities)
    yield put(userSave(newUser))
    yield put(activityTrackSuccess())
  } catch (error) {
    yield put(activityTrackFailure(error))
  }
}

/**
 * Export saga.
 */
export default function* testMainSaga() {
  yield all([
    takeEvery(types.ACTIVITY.TRACK.REQUEST, activityTrackSaga),
    takeEvery(types.ACTIVITY.SCHEDULE.REQUEST, activityScheduleSaga),
  ])
}
