import {
  all,
  call,
  put,
  takeEvery,
} from 'redux-saga/effects'

import {
  types,
  initSuccess,
  initFailure,
  userSave
} from '../actions'

import auth from '../services/auth'
import firestore from '../services/firestore'
import utils from '../../utils'

function* initSaga() {
  try {
    const anonymous = yield call(auth.signup)
    const id = anonymous.user.uid
    const data = yield call(firestore.get, {
      collection: 'activities',
      id
    })
    let user = data
    if (!user) {
      user = {}
      user.id = id
      user.scheduled = []
      user.activities = []
    } else {
      user = utils.getUser(id, user.activities)
    }
    yield put(userSave(user))
    yield put(initSuccess())
  } catch (error) {
    yield put(initFailure(error))
  }
}

/**
 * Export saga.
 */
export default function* testMainSaga() {
  yield all([
    takeEvery(types.INIT.REQUEST, initSaga),
  ])
}
