import {
  all,
  fork,
} from 'redux-saga/effects'

import init from './init'
import activity from './activity'

export default function* mainSaga() {
  yield all([
    fork(init),
    fork(activity),
  ])
}
