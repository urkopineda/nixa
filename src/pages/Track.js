import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Track.css'

import {
  connect,
} from 'react-redux'

import {
  withRouter,
} from 'react-router-dom'

import {
  activityTrackRequest,
} from '../redux/actions'

import Assets from '../assets'
import activities from '../activities.json'

class Track extends Component {
  state = {
    activity: Object.keys(activities)[0],
    seconds: 0,
    pause: false,
    stop: false
  }

  componentDidMount = () => {
    const { match } = this.props
    this.setState({
      activity: match.params.activity
    })
    this.updateTimer()
  }

  componentWillUnmount = () => {
    const { activity, seconds } = this.state
    const { user, activityTrackRequestDispatcher } = this.props
    if (seconds) {
      this.setState({
        pause: true,
        stop: true,
        seconds: 0,
      })
      activityTrackRequestDispatcher(user, activity, seconds)
    }
  }

  updateTimer = () => {
    setTimeout(() => {
      const { seconds, pause, stop } = this.state
      if (!pause && !stop) {
        this.setState({
          seconds: seconds + 1
        })
        this.updateTimer()
      }
    }, 1000)
  }

  getTimer = () => {
    const { seconds: now } = this.state
    const minutes = `0${Math.floor(now / 60)}`.slice(-2)
    const seconds = `0${now - minutes * 60}`.slice(-2)
    return `${minutes}:${seconds}`
  }

  setStop = () => {
    const { activity, seconds } = this.state
    const { user, activityTrackRequestDispatcher } = this.props
    this.setState({
      seconds: 0,
      pause: true,
      stop: true
    })
    activityTrackRequestDispatcher(user, activity, seconds)
  }

  setPause = () => {
    const { pause } = this.state
    const next = !pause
    this.setState({
      pause: next,
      stop: false
    })
    if (!next) {
      this.updateTimer()
    } 
  }

  render = () => {
    const { history } = this.props
    const { activity, pause, stop } = this.state
    return (
      <React.Fragment>
        <div className="tr-full-background" style={{
          backgroundImage: `url(${Assets.backgrounds[activity]})`
        }} />
        <div className="tr-full">
          <img className="tr-back" src={Assets.icon.back} alt="Back" onClick={() => history.goBack()} />
          <div className="tr-content">
            <p className="tr-ca">
              Current activity
            </p>
            <div className="tr-activity-icon-container">
              <img className="tr-activity-icon" src={Assets.icon[activity]} alt={activity} />
            </div>
            <p className="tr-ca-name">
              {activities[activity].title}
            </p>
            <div className="tr-time-container">
              <p className="tr-time">
                {this.getTimer()}
              </p>
              <hr className="tr-time-separator" />
              <p className="tr-subtime">
                Minutes
              </p>
            </div>
            <p className="tr-state">
              Activity Tracking
            </p>
            <p className="tr-state">
              {
                (stop)
                  ? 'Stopped'
                  : (pause)
                    ? 'Paused'
                    : 'In Progress...'
              }
            </p>
            <div className="tr-actions">
              <div className="tr-action-container" onClick={this.setPause}>
                {
                  (pause)
                    ? <div className="tr-play" />
                    : (
                      <div className="tr-pause-container">
                        <div className="tr-pause" />
                        <div className="tr-pause-divider" />
                        <div className="tr-pause" />
                      </div>
                    )
                }
              </div>
              <div className="tr-action-divider" />
              <div className="tr-action-container" onClick={this.setStop}>
                <div className="tr-stop" />
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user.user
})

const mapDispatchToProps = {
  activityTrackRequestDispatcher: activityTrackRequest
}

Track.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  activityTrackRequestDispatcher: PropTypes.func.isRequired,
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Track)
)
