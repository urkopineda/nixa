import React, { Component } from 'react'
import './Home.css'

import Header from '../organisms/Header'
import TrackYourActivity from '../organisms/TrackYourActivity'
import ScheduledActivities from '../organisms/ScheduledActivities'
import ActivityHistory from '../organisms/ActivityHistory'

class Home extends Component {
  render = () => {
    return (
      <React.Fragment>
        <Header />
        <TrackYourActivity />
        <ScheduledActivities />
        <ActivityHistory />
      </React.Fragment>
    )
  }
}

export default Home
