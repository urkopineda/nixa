import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Schedule.css'

import {
  connect,
} from 'react-redux'

import {
  withRouter,
} from 'react-router-dom'

import {
  activityScheduleRequest,
} from '../redux/actions'

import Assets from '../assets'
import utils from '../utils'
import activities from '../activities.json'
import slots from '../slots.json'

class Schedule extends Component {
  state = {
    active: undefined,
    long: 15,
    date: undefined,
    time: undefined,
    show: false,
    slot: false,
  }

  handleHide = () => {
    const { date, time } = this.state
    if (date && time) {
      this.setState({
        show: false
      })
    }
  }

  handleShow = () => {
    this.setState({
      show: true,
      date: undefined,
      time: undefined
    })
  }

  handleBack = () => {
    const { history } = this.props
    history.goBack()
  }

  handleClickSchedule = () => {
    const { user, history, activityScheduleRequestDispatcher } = this.props
    const { active, long, date, time, slot } = this.state
    let dt
    if (slot) {
      const datetime = document.getElementById('select-slot').value
      dt = new Date()
      dt.setTime(datetime)
    } else {
      dt = new Date(`${date} ${time}`)
    }
    activityScheduleRequestDispatcher(user, active, dt, long * 60)
    history.goBack()
  }

  handleChange = (id, value) => () => {
    this.setState({
      [(id)]: value
    })
  }

  handleChangeEvent = (id) => (event) => {
    this.handleChange(id, event.target.value)()
  }

  getDateTime = () => {
    const { date, time } = this.state
    const datetime = new Date(`${date} ${time}`)
    return utils.getDateString(datetime)
  }

  getMinDateTime = () => {
    const datetime = new Date()
    const year = datetime.getFullYear()
    const month = datetime.getMonth() + 1
    const day = datetime.getDate()
    return `${year}-${`0${month}`.slice(-2)}-${`0${day}`.slice(-2)}`
  }

  getDateTimeSlots = () => {
    const { long } = this.state
    const datetime = new Date()
    let parsed = []
    for (let day of Object.keys(slots)) {
      let date = new Date(datetime.getTime())
      for (let i = 0; i < 7; i++) {
        const w = date.toLocaleString('en-us', {
          weekday: 'long'
        })
        if (w === day) {
          break
        } else {
          date.setDate(date.getDate() + 1)
        }
      }
      if (datetime.getTime() !== date.getTime()) {
        date.setHours(0)
        date.setMinutes(0)
        date.setSeconds(0)
      }
      parsed.push({
        day,
        value: slots[day],
        date
      })
    }
    parsed = parsed.sort((a, b) => {
      if (a.date.getTime() > b.date.getTime()) {
        return 1
      }
      if (a.date.getTime() < b.date.getTime()) {
        return -1
      }
      return 0
    })
    const values = []
    for (let date of parsed) {
      const d = date.date
      for (let lapse of date.value) {
        const now = new Date(d.getTime())
        const start = new Date(d.getTime())
        const end = new Date(d.getTime())
        start.setHours(lapse.Start.split(':')[0])
        start.setMinutes(lapse.Start.split(':')[1])
        start.setSeconds(0)
        end.setHours(lapse.End.split(':')[0])
        end.setMinutes(lapse.End.split(':')[1])
        end.setSeconds(0)
        while (start.getTime() <= end.getTime()) {
          const temp = new Date(start.getTime())
          temp.setTime(temp.getTime() + long * 60000)
          if (temp.getTime() <= end.getTime() && temp.getTime() > now.getTime()) {
            values.push({
              text: utils.getDateString(start),
              value: start.getTime()
            })
          }
          start.setTime(start.getTime() + long * 60000)
        }
      }
    }
    return values
  }

  getLong = () => {
    const values = []
    for (let i = 0; i < 12; i++) {
      const count = 15 * (i + 1)
      const hours = Math.floor(count / 60)
      const minutes = `0${count - hours * 60}`.slice(-2)
      if (!hours) {
        values.push({
          text: `${minutes}min`,
          value: count
        })
      } else {
        values.push({
          text: `${hours}h ${minutes}min`,
          value: count
        })
      }
    }
    return values
  }

  render = () => {
    const { active, long, date, time, show, slot } = this.state

    return (
      <div className="sc-container">
        <img className="sc-close" src={Assets.icon.close} alt="Close" onClick={this.handleBack} />
        <p className="sc-title">
          Schedule your activity
        </p>
        <div className="sc-activities">
          {Object.keys(activities).map((activity) => (
            <div key={activity}>
              <div
                className={`sc-activity-icon-container ${(activity === active) ? 'sc-active' : ''}`}
                onClick={this.handleChange('active', activity)}
              >
                <img
                  className="sc-activity-icon"
                  src={
                    (activity === active)
                      ? Assets.icon[`${activity}_w`]
                      : Assets.icon[activity]
                  }
                  alt={activity}
                />
              </div>
              <p className="sc-activity-name">
                {activities[activity].title}
              </p>
            </div>
          ))}
        </div>
        <div className="sc-select-container">
          <p className="sc-select-title">
            How long do you want to do this activity?
          </p>
          <select
            className="sc-select"
            style={{
              backgroundImage: `url(${Assets.icon.dropdown_background}), linear-gradient(to bottom, #ffffff 0%, #ffffff 100%)`
            }}
            onChange={this.handleChangeEvent('long')}
          >
            {this.getLong().map((value) =>
              <option key={value.value} value={value.value}>
                {value.text}
              </option>
            )}
          </select>
        </div>
        <div className="sc-select-container">
          <p className="sc-select-title">
            When do you want to do this activity?
          </p>
          {
            (slot)
              ? (
                <select
                  id="select-slot"
                  className="sc-select"
                  style={{
                    backgroundImage: `url(${Assets.icon.search_background}), linear-gradient(to bottom, #ffffff 0%, #ffffff 100%)`
                  }}
                >
                  {this.getDateTimeSlots().map((value) =>
                    <option key={value.value} value={value.value}>
                      {value.text}
                    </option>
                  )}
                </select>
              )
              : (
                <div className="sc-input-container">
                  {
                    (!show)
                      ? (
                        <p className="sc-input" onClick={this.handleShow}>
                          {
                            (date && time)
                              ? this.getDateTime()
                              : 'Pick a date & time or find a free slot'
                          }
                        </p>
                      )
                      : (
                        <React.Fragment>
                          <input
                            className="sc-input-date"
                            type="date"
                            min={this.getMinDateTime()}
                            onChange={this.handleChangeEvent('date')}
                            onBlur={this.handleHide}
                          />
                          <input
                            className="sc-input-time"
                            type="time"
                            onChange={this.handleChangeEvent('time')}
                            onBlur={this.handleHide}
                          />
                        </React.Fragment>
                      )
                  }
                  <div className="sc-input-search-container" onClick={() => this.handleChange('slot', true)()}>
                    <img className="sc-input-search" src={Assets.icon.search} alt="Find a free slot" />
                  </div>
                </div>
              )
          }
        </div>
        <div className="sc-button-container">
          <button
            className={`sc-button ${(active && long && ((date && time) || slot)) ? '' : 'sc-button-disabled'}`}
            onClick={this.handleClickSchedule}
            disabled={!(active && long && ((date && time) || slot))}
          >
            <p className="sc-button-text">
              Schedule activity
            </p>
          </button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user.user
})

const mapDispatchToProps = {
  activityScheduleRequestDispatcher: activityScheduleRequest
}

Schedule.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  activityScheduleRequestDispatcher: PropTypes.func.isRequired,
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Schedule)
)
