import React, { Component } from 'react'
import PropTypes from 'prop-types'

import {
  connect,
} from 'react-redux'

import {
  withRouter,
} from 'react-router-dom'

import {
  initRequest,
} from './redux/actions'

import Router from './router'

class App extends Component {
  componentDidMount = () => {
    const { initRequestDispatcher } = this.props
    initRequestDispatcher()
  }

  render = () => {
    const { loading, success, failure } = this.props

    return (
      (!loading && success && !failure)
        ? <Router />
        : 'Loading...'
    )
  }
}

const mapStateToProps = (state) => ({
  loading: state.init.loading,
  success: state.init.success,
  failure: state.init.failure,
})

const mapDispatchToProps = {
  initRequestDispatcher: initRequest
}

App.propTypes = {
  loading: PropTypes.bool.isRequired,
  success: PropTypes.bool.isRequired,
  failure: PropTypes.bool.isRequired,
  initRequestDispatcher: PropTypes.func.isRequired,
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(App)
)
