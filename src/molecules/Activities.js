import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Activities.css'

import Assets from '../assets'

const MINIMUN_ITEMS = 5

class Activities extends Component {
  parseActivities = (activities) => {
    const ordered = activities.sort((a, b) => {
      const aDate = new Date(a.timestamp.seconds * 1000)
      const bDate = new Date(b.timestamp.seconds * 1000)
      if (aDate.getTime() > bDate.getTime()) {
        return 1
      }
      if (aDate.getTime() < bDate.getTime()) {
        return -1
      }
      return 0
    })
    const parsed = {}
    for (let activity of ordered) {
      const timestamp = new Date(activity.timestamp.seconds * 1000)
      const month = timestamp.toLocaleString('en-us', {
        month: 'long'
      })
      const weekday = timestamp.toLocaleString('en-us', {
        weekday: 'long'
      })
      const time = timestamp.toLocaleString('en-US', {
        hour: '2-digit',
        minute: '2-digit',
        hour12: true
      })
      const day = timestamp.toLocaleString('en-us', {
        day: 'numeric'
      })
      const key = `${month} ${day}-${weekday}`
      if (!parsed[key]) {
        parsed[key] = []
      }
      parsed[key].push({
        type: activity.type,
        icon: Assets.icon[activity.type],
        time
      })
    }
    for (let key of Object.keys(parsed)) {
      let count = MINIMUN_ITEMS - parsed[key].length
      if (count > 0) {
        parsed[key] = parsed[key].concat([...new Array(count)])
      }
    }
    return parsed
  }

  renderActivities = (activities) => {
    return (
      <div>
        {Object.keys(activities).map((key) => {
          const values = key.split('-')
          const title = values[0]
          const subtitle = values[1]
          return (
            <div key={key}>
              <p className="activities-title margin">
                {title}
              </p>
              <p className="activities-subtitle margin">
                {subtitle}
              </p>
              <div className="subactivities">
                {activities[key].map((activity, i) =>
                  (activity)
                    ? (
                      <div className="activity" key={i}>
                        <div className="activity-icon-container">
                          <img className="activity-icon" src={activity.icon} alt={activity.type} />
                        </div>
                        <p className="activity-time">
                          {activity.time}
                        </p>
                      </div>
                    )
                    : (
                      <div className="no-activity" key={i} />
                    )
                )}
                <div className="ah-margin" />
              </div>
            </div>
          )
        })}
      </div>
    )
  }

  render = () => {
    const { activities, noActivityText } = this.props
    const parsed = this.parseActivities(activities)
    return (
      parsed && Object.keys(parsed).length
        ? this.renderActivities(parsed)
        : (
          <p className="ah-subtitle margin">
            {noActivityText}
          </p>
        )
    )
  }
}

Activities.propTypes = {
  activities: PropTypes.arrayOf(PropTypes.object).isRequired,
  noActivityText: PropTypes.string.isRequired,
}

export default Activities