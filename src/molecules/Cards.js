import React from 'react'
import PropTypes from 'prop-types'
import './Cards.css'

import Card from '../atoms/Card'

const Cards = ({ data }) => (
  <div className="cards-container">
    {data.map((card, i) => (
      <Card
        key={i}
        image={card.image}
        icon={card.icon}
        title={card.title}
        subtitle={card.subtitle}
        onClick={card.onClick}
      />
    ))}
    <div className="cards-margin" />
  </div>
)

Cards.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object)
}

export default Cards
