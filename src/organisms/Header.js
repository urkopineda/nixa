import React from 'react'
import './Header.css'

import Assets from '../assets'

const Header = () => (
  <div className="h-header">
    <img className="h-logo" src={Assets.logo} alt="Tractiv" />
  </div>
)

export default Header
