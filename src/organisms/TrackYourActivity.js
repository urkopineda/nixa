import React from 'react'
import './TrackYourActivity.css'
import { withRouter } from 'react-router-dom'

import Assets from '../assets'
import activities from '../activities.json'

import Title from '../atoms/Title'
import Cards from '../molecules/Cards'

const TrackYourActivity = ({ history }) => (
  <div className="tya-container">
    <Title centered>
      Track your activity
    </Title>
    <Cards
      data={Object.keys(activities).map((key) => ({
        image: Assets[key],
        icon: Assets.icon[key],
        title: activities[key].title,
        subtitle: activities[key].subtitle,
        onClick: () => history.push(`/track/${key}`)
      }))}
    />
  </div>
)

export default withRouter(TrackYourActivity)
