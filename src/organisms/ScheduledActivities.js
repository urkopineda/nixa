import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './ScheduledActivities.css'

import {
  connect,
} from 'react-redux'

import {
  withRouter,
} from 'react-router-dom'

import Assets from '../assets'

import Activities from '../molecules/Activities'

class ScheduledActivities extends Component {
  render = () => {
    const { user, history } = this.props
    return (
      <div>
        <h2 className="sa-title">
          Scheduled activities
        </h2>
        <Activities
          activities={user.scheduled}
          noActivityText="You don’t have any activities scheduled yet."
        />
        <div className="sa-button-container">
          <button className="sa-button" onClick={() => history.push('/schedule')}>
            <img className="sa-button-icon" src={Assets.icon.plus} alt="Schedule activity" />
            <p className="sa-button-text">
              Schedule activity
            </p>
          </button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user.user
})

const mapDispatchToProps = {}

ScheduledActivities.propTypes = {
  user: PropTypes.object.isRequired,
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ScheduledActivities)
)

