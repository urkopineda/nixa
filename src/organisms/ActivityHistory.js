import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './ActivityHistory.css'
import {
  connect,
} from 'react-redux'

import Activities from '../molecules/Activities'

class ActivityHistory extends Component {
  render = () => {
    const { user } = this.props
    return (
      <div>
        <h2 className="ah-title">
          Activity history
        </h2>
        <Activities
          activities={user.activities}
          noActivityText="You haven’t tracked any activities yet."
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user.user
})

const mapDispatchToProps = {}

ActivityHistory.propTypes = {
  user: PropTypes.object.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(ActivityHistory)
