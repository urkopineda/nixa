import React from 'react'
import { Switch, Route, Redirect, withRouter } from 'react-router-dom'

import Home from './pages/Home'
import Track from './pages/Track'
import Schedule from './pages/Schedule'

const Router = () => (
  <Switch>
    <Route exact path='/' component={Home} />
    <Route exact path='/track/:activity' component={Track} />
    <Route exact path='/schedule' component={Schedule} />
    <Route path='*' render={() => <Redirect to='/' />} />
  </Switch>
)

export default withRouter(Router)