const getUser = (id, activities) => {
  const user = {}
  user.id = id
  user.activities = activities.sort((a, b) => {
    const aDate = new Date(a.timestamp.seconds * 1000)
    const bDate = new Date(b.timestamp.seconds * 1000)
    if (aDate.getTime() > bDate.getTime()) {
      return 1
    }
    if (aDate.getTime() < bDate.getTime()) {
      return -1
    }
    return 0
  })
  user.scheduled = user.activities.filter((activity) => {
    const date = new Date(activity.timestamp.seconds * 1000)
    return new Date().getTime() < date.getTime()
  })
  user.activities = user.activities.filter((activity) => {
    const date = new Date(activity.timestamp.seconds * 1000)
    return new Date().getTime() >= date.getTime()
  })
  return user
}

const getDateString = (datetime) => {
  const month = datetime.toLocaleString('en-us', {
    month: 'long'
  })
  const weekday = datetime.toLocaleString('en-us', {
    weekday: 'long'
  })
  const t = datetime.toLocaleString('en-US', {
    hour: '2-digit',
    minute: '2-digit',
    hour12: true
  })
  const day = datetime.toLocaleString('en-us', {
    day: 'numeric'
  })
  return `${weekday}, ${month} ${day} ${t}`
}

export default {
  getUser,
  getDateString
}