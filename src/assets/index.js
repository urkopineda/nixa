import logo from './logo.png'

import hiking from './hiking.png'
import spinning from './spinning.png'
import surfing from './surfing.png'
import weights from './weights.png'

import bg_hiking from './backgrounds/hiking.png'
import bg_spinning from './backgrounds/spinning.png'
import bg_surfing from './backgrounds/surfing.png'
import bg_weights from './backgrounds/weights.png'

import icon_plus from './icons/plus.png'
import icon_back from './icons/back.png'
import icon_close from './icons/close.png'
import icon_dropdown from './icons/dropdown.png'
import icon_search from './icons/search.png'
import icon_dropdown_background from './icons/dropdown-background.png'
import icon_search_background from './icons/search-background.png'

import icon_hiking from './icons/hiking.svg'
import icon_spinning from './icons/spinning.svg'
import icon_surfing from './icons/surfing.svg'
import icon_weights from './icons/weights.svg'

import icon_hiking_w from './icons/hiking_w.png'
import icon_spinning_w from './icons/spinning_w.png'
import icon_surfing_w from './icons/surfing_w.png'
import icon_weights_w from './icons/weights_w.png'

export default {
  logo,
  hiking,
  spinning,
  surfing,
  weights,
  backgrounds: {
    hiking: bg_hiking,
    spinning: bg_spinning,
    surfing: bg_surfing,
    weights: bg_weights,
  },
  icon: {
    plus: icon_plus,
    back: icon_back,
    close: icon_close,
    dropdown: icon_dropdown,
    search: icon_search,
    dropdown_background: icon_dropdown_background,
    search_background: icon_search_background,
    hiking: icon_hiking,
    spinning: icon_spinning,
    surfing: icon_surfing,
    weights: icon_weights,
    hiking_w: icon_hiking_w,
    spinning_w: icon_spinning_w,
    surfing_w: icon_surfing_w,
    weights_w: icon_weights_w,
  }
}
